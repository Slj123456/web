###### 玩转金刚梯>金刚字典>
### 注册
- 所请<strong> 注册 </strong>是指：[ 金刚号梯 ](/LadderFree/kkDictionary/KKLadderKKID.md)与[ 金刚VPN服务 ](/LadderFree/kkDictionary/KKServices.md)的潜在使用者通过完成在[ 金刚网 ](/LadderFree/kkDictionary/KKSiteZh.md)的登记、成为[ 金刚用户 ](/LadderFree/kkDictionary/KKUser.md)的过程
- <strong> 注册</strong>的实质是：
  - 潜在使用者与[ 金刚公司 ](/LadderFree/kkDictionary/Atozitpro.md)签订[ 《金刚VPN产品与VPN服务使用合约》 ](/LadderFree/kkDictionary/KKEnduserContract.md)，成为[ 金刚 ](/LadderFree/kkDictionary/Atozitpro.md)的签约[ 用户 ](/LadderFree/kkDictionary/KKUser.md)
  - 在《金刚网用户一览表》中创建了全新的一行，该行中记录了该[ 用户 ](/LadderFree/kkDictionary/KKUser.md)的某些信息，以区别于其他[ 用户 ](/LadderFree/kkDictionary/KKUser.md)

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)



