###### 玩转金刚梯>金刚字典>
### 网络电视机顶盒 苹果tvOS

- 网络电视苹果机顶盒 是[ 美国苹果公司 ](https://zh.m.wikipedia.org/zh-cn/%E8%98%8B%E6%9E%9C%E5%85%AC%E5%8F%B8)产品
- 苹果机顶盒的[ 操作系统 ](/LadderFree/kkDictionary/OS.md)是tvOS，由[ 美国苹果公司 ](https://zh.m.wikipedia.org/zh-cn/%E8%98%8B%E6%9E%9C%E5%85%AC%E5%8F%B8)研制
- tvOS是[ 闭源操作系统 ](/LadderFree/kkDictionary/OS_ClosedSourceOS.md)


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)


